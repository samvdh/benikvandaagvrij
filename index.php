<?php

$jaar = date('Y');

// beperking van easter_date()
if ($jaar < 1970 || $jaar > 2037) {
    $jaar = date('Y');
}

ini_set('date.timezone', 'Europe/Brussels');
$formatter = new \IntlDateFormatter('nl_BE', null, null);
$formatter->setPattern("D M y");

$format = function(\DateTime $datum) use ($formatter) {
    $formattedDatum = $formatter->format($datum);
    return $formattedDatum;
};

// vaste dagen
$nieuwjaar      = new \DateTime("$jaar-01-01");
$bevrijdingsdag = new \DateTime("$jaar-05-05");
$kerstmis       = new \DateTime("$jaar-12-25");
$tweedekerstdag = new \DateTime("$jaar-12-26");

$koningsdag = new \DateTime("$jaar-04-27");
// Als Koningsdag op zondag valt is het de dag ervoor
if ($koningsdag->format('w') === '0') {
    $koningsdag->sub(new DateInterval('P1D'));
}

// berekend
$pasen = new \DateTime();
$pasen->setTimestamp(\easter_date($jaar)); // bedankt PHP
$paasMaandag = clone $pasen;
$paasMaandag->add(new \DateInterVal('P1D')); // 1 dag na pasen
$goedeVrijdag = clone $pasen;
$goedeVrijdag->sub(new \DateInterval('P2D')); // 2 dag voor pasen
$olhHemelvaart = clone $pasen;
$olhHemelvaart->add(new \DateInterVal('P39D')); // 39 dagen na pasen
$pinksteren = clone $olhHemelvaart;
$pinksteren->add(new \DateInterVal('P10D')); // 10 dagen na OLH hemelvaart
$pinksterMaandag = clone $pinksteren;
$pinksterMaandag->add(new \DateInterVal('P1D')); // 1 dag na pinksteren

$vrijeDagen = array(
  array(
    "name" => "nieuwjaarsdag!!",
    "date" => $nieuwjaar->format('d m y'),
    "vrij" => true,
  ),
  array(
    "name" => "pasen!!",
    "date" => $pasen->format('d m y'),
    "vrij" => true,
  ),
  array(
    "name" => "pasen!!",
    "date" => $paasMaandag->format('d m y'),
    "vrij" => true,
  ),
  array(
    "name" => "koningsdag!!",
    "date" => $koningsdag->format('d m y'),
    "vrij" => true,
  ),
  array(
    "name" => "bevrijdingsdag!!",
    "date" => $bevrijdingsdag->format('d m y'),
    "vrij" => is_float($jaar / 5) ? false : true,
  ),
  array(
    "name" => "hemelvaart!!",
    "date" => $olhHemelvaart->format('d m y'),
    "vrij" => true,
  ),
  array(
    "name" => "pinksteren!!",
    "date" => $pinksteren->format('d m y'),
    "vrij" => true,
  ),
  array(
    "name" => "pinksteren!!",
    "date" => $pinksterMaandag->format('d m y'),
    "vrij" => true,
  ),
  array(
    "name" => "kerst!!",
    "date" => $kerstmis->format('d m y'),
    "vrij" => true,
  ),
  array(
    "name" => "kerst!!",
    "date" => $tweedekerstdag->format('d m y'),
    "vrij" => true,
  )
);

// checks for free day
$i = 0;
$vrij = false;
$reden = null;
foreach($vrijeDagen as $vrijeDag){
  $vandaag = date('d m y');

  if($vandaag == $vrijeDagen[$i]["date"] && $vrijeDagen[$i]["vrij"]){
    $vrij = true;
    $reden = $vrijeDagen[$i]["name"];
  }

  $i++;
}

//checks for weekend
if(date('w') == 0){
  if($reden === null){
    $reden = 'zondag!! Je mag nog heel even genieten ;)';
  }
  $vrij = true;
}
//checks for weekend
if(date('w') == 6){
  if($reden === null){
    $reden = 'zaterdag!!';
  }
  $vrij = true;
}

if($vrij){
  $work = '<span class="green">JAAA!</span> Je bent vandaag vrij!!';
} else {
  $work = '<span class="red">Nee.</span> Wat denk jij? Dat ik sinterklaas ben ofzo?';
}


//generates html
?>
<html>
  <head>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,700;1,400&display=swap" rel="stylesheet">
    <style>
      body {
        font-family: 'Roboto', sans-serif;
      }
      .infobox {
        width: 800px;
        max-width: 80vw !important;
        margin: 60px auto 30px;
        background: #eee;
        padding: 10px 30px 60px 30px;
      }
      .center {
        margin: 0 auto 60px;
        text-align: center;
      }
      .center {
        z-index: 100;
        position: relative;
      }
      img {
        max-width: 100%;
      }
      .green {
        color: green;
      }
      .red {
        color: red;
      }
    </style>
  </head>
  <body>
    <div class="infobox">
      <h1>Ben ik vandaag vrij?</h1>
      <h3><?php echo $work ?></h3>
      <?php
        if($vrij){
          echo '<h4>Het is '. $reden .'</h4>';
        }

        if(!$vrij){
          if(date('G') >= 9){
            echo '<h5>Het is godnondeju al <span class="red">9 uur</span> geweest. Aan de bak jij!</h5>';
          }
        }
      ?>
    </div> 

    <div class="center">
      <h5>Deze tool wordt mede mogelijk gemaakt door:</h5>
      <a href="https://system4.nl" target="_blank"><img width="300" src="logo-system4.png" alt="Logo System4" /></a>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="jquery.confetti.js"></script>
    <?php
      echo $vrij ? '<script>$.confetti.start()</script>' : null;
    ?>
  </body>
</html>